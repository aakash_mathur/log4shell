package com.example.springboot;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

//import org.apache.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@RestController
public class HelloController {
	private static final Logger logger = LogManager.getLogger(HelloController.class);
    
	@GetMapping("/")
	public String index(@RequestHeader Map<String, String> headers) {
		headers.forEach((key, value) -> {
	        logger.info(String.format("Header '%s' = %s", key, value));
	    });
		
		return headers.toString();
	}
	
	@GetMapping("/agent")
	public String agent(@RequestHeader("user-agent") String userAgent) {
		logger.info("User Agent:" + userAgent);
		
		return "This is your user agent: " + userAgent;
	}
}
